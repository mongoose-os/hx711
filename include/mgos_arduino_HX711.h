/*
 * Arduino HX711 library API wrapper
 */

#include <Arduino.h>
#include "HX711.h"

/*
 * Create a HX711 instance
 */
HX711 *mgos_HX711(void);

//virtual *~mgos_HX711();

/* 
 * Setup the instance with the data output pin, clock input pin and gain factor
 */
void mgos_begin(HX711 *hx, byte dout, byte pd_sck, byte gain = 128);

/* 
 * Check if HX711 is ready
 */
bool mgos_HX711_is_ready(HX711 *hx);

void mgos_HX711_wait_ready(HX711 *hx, unsigned long delay_ms = 0);

bool mgos_HX711_wait_ready_retry(HX711 *hx, int retries = 3, unsigned long delay_ms = 0);

bool mgos_HX711_wait_ready_timeout(HX711 *hx, unsigned long timeout = 1000, unsigned long delay_ms = 0);

void set_gain(byte gain = 128);

long mgos_HX711_read(HX711 *hx);

long mgos_HX711_read_average(HX711 *hx, byte times = 10);

double mgos_HX711_get_value(HX711 *hx, byte times = 1);

void mgos_HX711_tare(HX711 *hx, byte times = 10);

void mgos_HX711_set_scale(HX711 *hx, float scale = 1.f);

float mgos_HX711_get_scale(HX711 *hx);

void mgos_HX711_set_offset(HX711 *hx, long offset = 0);

long mgos_HX711_get_offset(HX711 *hx);

void mgos_HX711_power_down(HX711 *hx);

void mgos_HX711_power_up(HX711 *hx);
