#include "mgos_arduino_HX711.h"


HX711 *mgos_HX711(void){
	return new HX711();
}

/*
virtual *~mgos_HX711(void){
	return new ~HX711();
}
*/
void mgos_HX711_begin(HX711 *hx, byte dout, byte pd_sck, byte gain){
	if (hx == nullptr) return;
	hx->begin(dout, pd_sck, gain);
}

bool mgos_HX711_is_ready(HX711 *hx){
	if (hx == nullptr) return false;
	return hx->is_ready();
}

void mgos_HX711_wait_ready(HX711 *hx, unsigned long delay_ms){
	if (hx == nullptr) return;
	hx->wait_ready(delay_ms);
}


bool mgos_HX711_wait_ready_retry(HX711 *hx, int retries, unsigned long delay_ms){
	if (hx == nullptr) return false;
	return hx->wait_ready_retry(retries, delay_ms);
}

bool mgos_HX711_wait_ready_timeout(HX711 *hx, unsigned long timeout, unsigned long delay_ms){
	if (hx == nullptr) return false;
	return hx->wait_ready_timeout(timeout, delay_ms);
}	

void mgos_set_gain(HX711 *hx, byte gain){
	if (hx == nullptr) return;
	hx->set_gain(gain);	
}

long mgos_HX711_read(HX711 *hx){
	if (hx == nullptr) return 0;
	return hx->read();
}

long mgos_HX711_read_average(HX711 *hx, byte times){
	if (hx == nullptr) return 0;
	return hx->read_average(times);
}

double mgos_HX711_get_value(HX711 *hx, byte times){
	if (hx == nullptr) return 0;
	return hx->get_value(times);
}

void mgos_HX711_tare(HX711 *hx, byte times){
	if (hx == nullptr) return;
	hx->tare(times);
}

void mgos_HX711_set_scale(HX711 *hx, float scale){
	if (hx == nullptr) return;
	hx->set_scale(scale);
}

float mgos_HX711_get_scale(HX711 *hx){
	if (hx == nullptr) return 0;
	return hx->get_scale();
}

void mgos_HX711_set_offset(HX711 *hx, long offset){
	if (hx == nullptr) return;
	hx->set_offset(offset);
}

long mgos_HX711_get_offset(HX711 *hx){
	if (hx == nullptr) return 0;
	return hx->get_offset();
}

void mgos_HX711_power_down(HX711 *hx){
	if (hx == nullptr) return;
	hx->power_down();
}

void mgos_HX711_power_up(HX711 *hx){
	if (hx == nullptr) return;
	hx->power_up();
}
